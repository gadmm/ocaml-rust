/* Exemple borrowing container vs. borrowing contents */

struct Inspector<'a> {
    contents: &'a Box<u8>
}

/*idem: struct Inspector<'a> {
    contents: u8,
    _marker: std::marker::PhantomData<&'a ()>
}*/

impl<'a> Drop for Inspector<'a> {
    fn drop(&mut self) {
        println!("I was only {} days from retirement!", *(self.contents));
    }
}

fn f() {
    let mut days = Box::new(2);
    let _inspector = Inspector { contents: &days }; // (1)
    *(days.as_mut()) = 3; // (2)
    /* The intention is that `_inspector` borrows `days` but not the
       contents of `days`. Thus the mutation of its contents does not
       overlap and the destructor correctly seems the new value. */
}

/* we want to express that borrowing the contents of a box returns a
   different lifetime: */
fn borrow_box<'a,'b,T>(b:&'a mut Box<T>) -> &'b mut T
where 'a : 'b /* what relationship to put here? */ {
    b.as_mut()
}
/* - if (1) (above) stored the contents of the box, it must get a more
     constrained type than just storing the borrow to the container.
     It should not be possible to relax 'b into 'a in the function
     above.

   - if (2) mutated the box instead of the contents, it should detect
     that a borrowed elements is being mutated as before. So the
     important difference is that a mutation of 'b is not recorded as
     a mutation of 'a.

   This is better explained by making a distinction between "having
   'a" (a property of data, expressed by the type, distinction: linear
   vs. non-linear) and "accessing 'a" (a property of code, expressed
   as an effect annotation, distinction: read vs. write).

   We need to have a relationship 'a > 'b which means:
   1. a mutation of &mut 'b does not count as a mutation at 'a. In
      other words, having 'a does not count as having 'b.
   2. an access of 'a is (potentially) a creation of 'b, thus subject
      to aliasing rules:
      - an access of &mut 'a must only happen only after no more 'b is
        used (a read can break non-aliasing, a write can invalidate
        the data at 'b).
      - an access of &'a must only happen only if all borrows at 'b
        are non-linear.

   In particular:
   - a (mutable) borrow of 'a can exist while a (mutable) borrow of
     'b exists and the latter is accessed, without this breaking the
     aliasing rule.
   - the source of 'a must live longer than 'b (since destroying it
     can be seen as an access of &mut 'a). */

fn main() {
    f()
}
