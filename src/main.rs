use std::cell::UnsafeCell;
use std::marker::PhantomData;
use std::ops::Deref;

/// I. Runtime capability and type of OCaml values

/// I.1. The runtime capability

pub struct Runtime {
    _private: (),
}

impl Runtime {
    pub const unsafe fn create() -> Self {
        Runtime { _private: () }
    }

    pub unsafe fn create_mut() -> &'static mut Runtime {
        static mut GC : Runtime = unsafe { Runtime::create() };
        &mut GC
    }
}

/// I.2. OCaml unrooted values (the C [value] type).

// Values are always safe for reading transitively; in particular they
// possess a read-only capability for runtime access if they
// transitively point to the OCaml heap.
#[derive(Copy, Clone)]
#[repr(C)]
pub struct Value<'a> {
    raw: usize,
    _marker: PhantomData<&'a Runtime>
}

impl<'a> Value<'a> {
    pub unsafe fn create(raw: usize) -> Value<'a> {
        Value {
            raw: raw,
            _marker: PhantomData
        }
    }

    pub unsafe fn from_raw(_gc: &'a Runtime, raw: usize) -> Value<'a> {
        Value::create(raw)
    }

    pub unsafe fn from_ptr(gc: &Runtime, raw: usize) -> Value {
        // OCaml pointers must be 4-aligned
        assert!(raw & 3 == 0);
        Value::from_raw(gc, raw)
    }

    // Note that immediateness of OCaml values can be expressed with
    // the 'static lifetime. No need to possess the runtime capability
    // to read their contents! This allows to express the trick where
    // one avoids rooting immediates.
    pub fn from_int(int: usize) -> Value<'static> {
        assert!(int <= usize::max_value() / 2);
        Value {
            raw: (int << 1) + 1,
            _marker: PhantomData
        }
    }

    pub fn is_immediate(&self) -> bool {
        self.raw & 1 == 1
    }

    pub fn assert_runtime_capability(&self) {
        debug_assert!(!self.is_immediate());
    }

    pub fn size(&self) -> usize {
        if self.is_immediate() {
            return 0;
        }
        // let us pretend that the header directly gives the size
        let ptr = self.raw as *const usize;
        unsafe {
            self.assert_runtime_capability();
            ptr.offset(-1).read()
        }
    }

    // Note: one should also check the tag; some tags indicate that it
    // contains something else than OCaml values. (e.g., float tag,
    // custom tag, closure tag).
    pub fn get(&self, offset: usize) -> Value<'a> {
        assert!(offset < self.size());
        let ptr = self.raw as *const usize;
        unsafe {
            self.assert_runtime_capability();
            let raw = ptr.offset(offset as isize).read();
            Value::create(raw)
        }
    }
}


/// I.3. Representing Values via a reference

// Sometimes it is convenient to forget the lifetime annotation on the
// value, for instance when the lifetime is already present via a
// borrow. RawValue must obey the invariant that one can safely obtain
// a Value<'a> from a &'a RawValue. Essentially, only safe inside
// special containers below.
pub struct RawValue {
    raw: usize
}

impl RawValue {
    pub unsafe fn from_value<'a>(value: Value<'a>) -> RawValue {
        RawValue { raw: value.raw }
    }

    pub fn as_value<'a>(&'a self) -> Value<'a> {
        unsafe { Value::create(self.raw) }
    }
}


/// I.4. Expressing the OCaml API calling convention for functions
///      that take no value argument and for non-allocating functions

// Example of an allocating function in the OCaml API calling
// convention, but without input.
pub fn caml_alloc_pair<'a>(gc: &'a mut Runtime) -> Value<'a> {
    // Let us pretend that this allocates the cell of a pair
    unsafe { Value::from_raw(gc, 1) }
}

// Problem 1: &mut Runtime has representation usize, it would be
// better if it was () (we are not interested in Option<&Runtime>
// being of the same size as &Runtime).
//
// Non-solution: use a PhantomData<&mut> instead of a borrow (not
// explored here). After trying this, I had to implement coercions (to
// immutable variant) by hand, and then reborrowing by hand which did
// not work. (My understanding is that PhantomData<&mut ()> and
// PhantomData<& ()> induce the same constraints; they cannot be used
// to qualify the mutability or immutability of the phantom lifetime;
// it only defines variance and drop-check. See:
// <https://doc.rust-lang.org/nomicon/phantom-data.html>)
//
// Solution:
// - accept the overhead (less boilerplate), or
// - have a token that one has to immediately convert into a &mut
// Runtime


// Example of a non-allocating function in the OCaml API calling
// convention
pub fn caml_noalloc_read<'a>(_gc: &'a Runtime, x: Value<'a>) -> Value<'a> {
    x.get(0)
}


/// II. Rooting: a Box-like movable global root

// An OCaml globally rooted value. Necessary for later examples, and
// simpler to express in a small example than local roots on the stack
// (famous last words).
pub struct BoxRoot {
    root: Box<UnsafeCell<Value<'static>>>
}

pub unsafe fn caml_register_global_root(_gc: &Runtime, _ptr: *mut Value) {}
pub unsafe fn caml_remove_global_root(_gc: &Runtime, _ptr: *mut Value) {}

impl BoxRoot {
    // We do not need the runtime capability: either val is immediate
    // and there is no need for rooting, or we already have read-only
    // access to the runtime.
    pub fn create<'a>(val: Value<'a>) -> BoxRoot {
        let r = unsafe {
            let r = Box::new(UnsafeCell::new(Value::create(val.raw)));
            if !val.is_immediate() {
                val.assert_runtime_capability();
                caml_register_global_root(&Runtime::create(), r.get());
            };
            r
        };
        BoxRoot { root: r }
    }

    pub fn get<'a>(&self, _gc: &'a Runtime) -> Value<'a> {
        unsafe { *(*self.root).get() }
    }
}

impl Drop for BoxRoot {
    fn drop (&mut self) {
        unsafe {
            // interesting problem: where to find the runtime
            // capability?
            caml_remove_global_root(&Runtime::create(), self.root.get());
        };
    }
}


// Problem 2 (digression!): drop should only run while one possesses
// the runtime lock. It is unsafe to drop a BoxRoot in a parallel
// thread, or after the runtime has been shut down.
//
// Solutions:
//
// - do not call caml_remove_global_root in drop and have an explicit
// clean-up operation (linear type). Make the type must_use to warn
// when the explicit cleanup is forgotten. Leaks in case of panic.
//
// - have a queue of roots pending for removal, that is flushed when
// acquiring the runtime lock. But there is no reason to move an
// BoxRoot inside blocking sections, since its contents cannot be
// read without the runtime capability, so this queue is practically
// useless.
//
// - record in a boolean whether we have the runtime lock and
// dynamically decide whether to leak or to remove the root. Leaking
// is safe, and is unlikely (?) to happen for practical code, for the
// reason mentioned previously.
//
// - reimplement global roots in a way that is independent from the
// OCaml runtime (e.g. with a scanning hook registered at
// startup/shutdown).



/// III. A Callee-roots calling convention for allocating functions


/// III.1. Expressing dependency with a dependent pair (“sigma type”)

// Problem 3: How to pass a value alongside the mutable capability
//
// Solution: Use a sigma-type such as owning_ref::OwningRef.


// Problem 4: OwningRef contains and dereferences into a pointer &'b
// Value instead of Value<'b>
//
// Solution: Use the variant owning_ref::OwningHandle that holds an
// object.


// OwningHandle unnecessarily requires the Deref trait for the handle,
// so we add a dummy container.
pub struct Container<T> {
    contents: T
}

impl<T> Deref for Container<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.contents
    }
}

type RuntimeAndValue<'a> =
    owning_ref::OwningHandle<&'a mut Runtime, Container<RawValue>>;
// This expresses that the RawValue depends on the &mut Runtime.

// Note that its representation is (*mut (), usize), so
// modulo Problem 1 we are zero-cost.


/// III.2. A single-argument prototype of a callee-roots function

pub fn caml_make_a_pair<'a>(sigma: RuntimeAndValue<'a>) -> Value<'a> {

    // We do some noalloc stuff before rooting the values... because
    // we can. First, we readily have the read-only capability.
    let gc_immut : &Runtime = sigma.as_owner();
    // Note: this is not [&'a Runtime] (crucially!)

    let x : Value = sigma.as_value();
    let y : Value = caml_noalloc_read(gc_immut, x);
    // Note: this is not [Value<'a>] (crucially!)

    // Could be done as a macro. Essentially, CAMLparam(x) amounts to
    // the following:
    // ```
    //   let x_root = BoxRoot::create(sigma.as_owner(), sigma.as_value());
    //   let gc = sigma.into_owner();
    // ```
    let x_root : BoxRoot = BoxRoot::create(x);
    let y_root : BoxRoot = BoxRoot::create(y);
    let gc : &mut Runtime = sigma.into_owner();
    // Note: now this is [&'a mut Runtime] (crucially!)

    let pair : Value = caml_alloc_pair(gc);
    //caml_alloc_pair(gc); // Compilation error: good

    // Necessary to avoid compilation error: bad (see Problem 5 below)
    let pair_rooted = BoxRoot::create(pair); //1
    let pair = pair_rooted.get(gc);            //2

    //let y : Value = caml_noalloc_read(gc_immut, x); // Compilation error: good
    //let y : Value = caml_noalloc_read(gc, x); // Compilation error: good
    //let y : Value = caml_noalloc_read(gc_immut, y_root.get(gc)); // Compilation error: good

    // Let us pretend that we save x and y into pair
    let _x : Value<'a> = caml_noalloc_read(gc, x_root.get(gc));
    let _y : Value<'a> = caml_noalloc_read(gc, y_root.get(gc));

    pair
    // Drop x_root, y_root, pair_rooted
}


// Problem 5: With the signature
// ```
//   pub fn caml_alloc_pair<'a>(gc: &'a mut Runtime) -> Value<'a>
// ```
// it thinks that the result of caml_alloc_pair keeps a
// mutable borrow of the gc (comment the lines marked 1 and 2 above).
//
// This is illustrated in a MWE here:
// https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=feecb618fa2436ad64f0c932ea0e369d
//
// I tried various workarounds to no avail. See also PhantomData<&mut
// ()> and PhantomData<& ()> defining identical constraints.
//
// Solution:
//
// - Accept that the returned temporary value in this case must
// immediately be rooted. This diminishes the expressiveness of the
// no-alloc dialect a bit, but does not negate it.
//
// - Root the value and re-borrow, to convince Rust that the borrow
// has been downgraded to an immutable one (i.e. the previous
// expressiveness limitation can be lifted simply by adding a spurious
// root)
//
// ```
//   let pair_rooted = BoxRoot::create(pair);
//   let pair = pair_rooted.get(gc);
// ```
//
// - Same as previous, but using unsafe functions to avoid the
// rooting. Better suited inside macros in case the latter have to be
// used.
//
// ```
//  let pair = unsafe {
//      let raw = pair.raw;
//      Value::from_raw(gc, raw)
//  };
// ```


// Example of packing arguments into a sigma type to call a
// callee-roots function from a caller-roots function.
pub fn call_callee_roots<'a, 'b>(gc: &'a mut Runtime, value: &'b BoxRoot)
                                 -> BoxRoot
{
    let value : Value = value.get(gc);
    // Boilerplate to pack gc and value into a sigma
    let sigma =
        unsafe {
            let handle = Container { contents: RawValue::from_value(value) };
            // reborrow explicitly (otherwise error below due to moving [gc])
            let gc2 : &mut _ = gc;
            owning_ref::OwningHandle::new_with_fn(gc2, |_| { handle })
        };
    let v = caml_make_a_pair(sigma);
    //caml_alloc_pair(gc); // Compilation error: good
    let res = BoxRoot::create(v);
    caml_alloc_pair(gc); // Control: no compilation error: good (gc still available)
    res
}


/// III.3. Unrestricted callee-roots functions

// Problem 6: This is clumsy for passing several Values, or a mix of
// Values and non-Values as argument.
//
// Solution: decouple the borrow from the payload. See for instance
// qcell::LCell, which looks like what we are looking for.

type ValueArg<'id> = qcell::LCell<'id, RawValue>;

// representation: usize
static_assertions::assert_eq_size!(ValueArg, usize);

// Now a hand-made owner that is 0-size
struct RuntimeContainer<'a> {
    _marker: PhantomData<&'a mut Runtime>
}

impl<'a> RuntimeContainer<'a> {
    pub fn from(_gc: &'a mut Runtime) -> RuntimeContainer<'a> {
        RuntimeContainer { _marker: PhantomData }
    }

    pub fn to(self) -> &'a mut Runtime {
        unsafe { Runtime::create_mut() }
    }
}

impl<'a> Deref for RuntimeContainer<'a> {
    type Target = Runtime;

    fn deref(&self) -> &Self::Target {
        unsafe { Runtime::create_mut() }
    }
}

unsafe impl<'a> owning_ref::StableAddress for RuntimeContainer<'a> {}

pub struct RuntimeAndValues<'a, 'id> {
    contents: owning_ref::OwningHandle<RuntimeContainer<'a>,
                                       Container<qcell::LCellOwner<'id>>>
}

// representation: ()
static_assertions::assert_eq_size!(RuntimeAndValues, ());

impl<'a,'id> RuntimeAndValues<'a, 'id> {

    pub fn immut_cap(&self) -> &Runtime {
        self.contents.as_owner()
    }

    pub fn into_cap(self) -> &'a mut Runtime {
        self.contents.into_owner().to()
    }

    pub fn extract_temp_value<'b>(&'b self, value: &'b ValueArg<'id>) -> Value<'b> {
        let owner : &qcell::LCellOwner<'id> = &self.contents;
        owner.ro(value).as_value()
    }
}

/// III.4. Example of callee-roots function with several arguments

pub fn caml_ultimate_func<'a, 'id>(gc: RuntimeAndValues<'a, 'id>,
                                   x: ValueArg<'id>,
                                   y: ValueArg<'id>,
                                   i: &mut usize) -> Value<'a> {

    // Some noalloc stuff before rooting the values
    let gc_immut : &Runtime = gc.immut_cap();
    let x : Value = gc.extract_temp_value(&x);
    let y : Value = gc.extract_temp_value(&y);
    caml_noalloc_read(gc_immut, y); // No compilation error: good
    *i = x.size() + y.size();

    // Now root the values and recover the mutable capability
    let x_root : BoxRoot = BoxRoot::create(x);
    let y_root : BoxRoot = BoxRoot::create(y);
    let gc : &'a mut Runtime = gc.into_cap();

    let pair : Value = caml_alloc_pair(gc);

    // Necessary to avoid compilation error: bad (see Problem 5)
    let pair = unsafe {
        let raw = pair.raw;
        Value::from_raw(gc, raw)
    };

    //caml_alloc_pair(gc); // Compilation error: good

    //caml_noalloc_read(gc_immut, x); // Compilation error: good
    //caml_noalloc_read(gc, x); // Compilation error: good
    //caml_noalloc_read(gc_immut, y_root.get(gc)); // Compilation error: good


    // Let us pretend that we save x and y into pair
    let _x : Value<'a> = caml_noalloc_read(gc, x_root.get(gc));
    let _y : Value<'a> = caml_noalloc_read(gc, y_root.get(gc));

    pair
}


// Example of wrapping of a callee-roots function into a caller-roots
// function.
pub fn call_ultimate<'a>(gc: &'a mut Runtime,
                         x: &BoxRoot,
                         y: &BoxRoot,
                         i: &mut usize)
                         -> BoxRoot
{
    let x : Value = x.get(gc);
    let y : Value = y.get(gc);

    // Obfuscated code contest: implementing the identity.
    let mut result = std::mem::MaybeUninit::<Value<'a>>::uninit();
    let result =
        unsafe {
            // Ensure:
            // 1. result is initialised
            // 2. x_raw and y_raw can only escape as borrows with lifetime 'a.
            let x_raw = RawValue::from_value(x);
            let y_raw = RawValue::from_value(y);
            let gc2 : &mut _ = gc;
            let gc2 : RuntimeContainer = RuntimeContainer::from(gc2);
            qcell::LCellOwner::scope(|owner| {
                let x_arg : ValueArg = owner.cell(x_raw);
                let y_arg : ValueArg = owner.cell(y_raw);
                let handle = Container { contents: owner };
                let sigma =
                    RuntimeAndValues {
                        contents: owning_ref::OwningHandle::new_with_fn(gc2, |_| { handle })
                    };
                let v = caml_ultimate_func(sigma, x_arg, y_arg, i);
                // Note: LCellOwner::Scope does not allow returning arguments
                result.as_mut_ptr().write(v);
            });
            result.assume_init()
        };
    // Let us forget that `result` borrows mutably (Problem 5)
    let result = unsafe {
        let raw = result.raw;
        Value::from_raw(gc, raw)
    };

    //caml_alloc_pair(gc); // Compilation error: good
    caml_noalloc_read(gc, result); // Control: no compilation error: good (gc still available read-only)

    let result = BoxRoot::create(result);

    caml_alloc_pair(gc); // Control: no compilation error: good (gc still available read-write)

    result
}


/// III.5. Introducing macros

// - Macros (moreover containing unsafe) will be *necessary* to
// implement the calling of callee-roots function (to introduce the
// sigma type).
//
// - Macros (not containing any unsafe) will be useful to 1) unpack
// arguments of callee-roots macros, 2) root them. (The equivalent of
// OCaml's CAMLparam.)

macro_rules! CAMLparam {
    ($gc:ident, $($x:ident),*) => (
        let $gc : RuntimeAndValues = $gc;
        $(
            let $x : Value = $gc.extract_temp_value(&($x));
            let $x = BoxRoot::create($x);
        )*
        let $gc : &mut Runtime = $gc.into_cap();
    )
}

macro_rules! recover_alloc_cap {
    ($gc:ident, $x:ident) => (
        let $x : Value = {
            let $x : Value = $x;
            unsafe {
                let raw = $x.raw;
                Value::from_raw($gc, raw)
            }
        };
    )
}

// Simple example
pub fn caml_ultimate_func2<'a, 'id>(gc: RuntimeAndValues<'a, 'id>,
                                    x: ValueArg<'id>,
                                    y: ValueArg<'id>,
                                    _i: &mut usize) -> Value<'a> {
    CAMLparam!(gc, x, y);
    let pair : Value = caml_alloc_pair(gc);
    // fix Problem 5
    recover_alloc_cap!(gc, pair);

    //caml_alloc_pair(gc); // Compilation error: good

    // Let us pretend that we save x and y into pair
    caml_noalloc_read(gc, x.get(gc));
    caml_noalloc_read(gc, y.get(gc));
    pair
}


// Trying to break the example
pub fn caml_ultimate_func3<'a, 'id>(gc: RuntimeAndValues<'a, 'id>,
                                    x: ValueArg<'id>,
                                    y: ValueArg<'id>,
                                    _i: &mut usize) -> Value<'a> {
    let _gc_ref = &gc; // oops, saving the capability for later
    let _y = gc.extract_temp_value(&y); // oops, saving y for later
    CAMLparam!(gc, x); // oops, forgot to root y!

    //_gc_ref.extract_temp_value(&y); // Compilation error: good

    //_y.get(0); // Compilation error: good

    x.get(gc)
}

// It would be interesting to write a macro for calling a callee-roots
// function (the hard part). One has to distinguish Values that have
// to be rooted from non-values (and maybe immediates).



/// IV. A Caller-roots calling convention

// Summary:
//
// The calling convention is about passing and returning ValueRefs,
// which are pointers to the OCaml C API `value` type. BoxRoot is an
// owned ValueRef. The distinction is the same as between passing
// borrows or passing ownership. The demo shows that the two suffice.
// (Note: this would not be the case with local roots alone as you
// cannot return them.)
//
// ValueRefs can indistinctly denote pointers to immediates, unrooted
// values, roots of various kinds, and direct pointers into the OCaml
// heap. But the borrowing rules will prevent you from passing
// anything else than immediates and roots to an allocating function,
// forcing you to root the values if necessary in that case.
//
// Optionally (more advanced?) users can define functions that work on
// Values directly. Values have the added convenience of not requiring
// the runtime capability to be manipulated (they already have it).
// This is a type that will already exist in the low-level API and
// will be used internally in any case. From an API perspective there
// is also the same distinction between passing Values and passing
// ValueRefs as between pass-by-value and pass-by-reference.
// Conversion between the two is seamless and anything accepted by the
// compiler is valid. The difference with callee-roots is that instead
// of trying to pass Values alongside the runtime capability to an
// allocating function, we will avoid that.
//
// ValueCell is a bit like UnsafeCell, used internally.



/// IV.1. ValueCell, a container for roots that can be mutated by the GC.

// Safety:
//
// 1. The contents must give a valid Value to whoever has read access
// to the runtime.
//
// 2. No mutation to the UnsafeCell can happen through a &ValueCell.
#[repr(C)]
pub struct ValueCell {
    cell: UnsafeCell<Value<'static>>
}

type ValueRef<'a> = &'a ValueCell;

static_assertions::assert_eq_size!(Value, ValueCell, usize);

impl ValueCell {
    pub unsafe fn create<'a>(val: Value<'a>) -> ValueCell {
        // coerce lifetimes
        let raw : Value<'static> = std::mem::transmute(val);
        ValueCell { cell : UnsafeCell::new(raw) }
    }

    pub fn value<'a>(&self, _gc: &'a Runtime) -> Value<'a> {
        unsafe { *self.cell.get() }
    }
}

// Example. We can now use ValueRef as a generic type for passing
// roots around. Here the UnsafeCell is there to express the fact that
// the GC can update it.

// Following the smart pointer analogy, we implement automatic
// coercion.

impl Deref for BoxRoot {
    type Target = ValueCell;

    fn deref(&self) -> ValueRef {
        let cell : &UnsafeCell<Value<'static>> = &self.root;
        // Note: if we had defined ValueRef before, we would not need
        // this conversion.
        unsafe { std::mem::transmute(cell) }
    }
}


/// IV.2. Caller-roots calling convention (but returning Value)

// As before we can define a macro to unpack arguments...
macro_rules! CAMLvalues {
    ($gc:ident, $($x:ident),*) => (
        $(
            let $x : Value = $x.value($gc);
        )*
    )
}

// A function that takes two arguments and does not perform allocations
pub fn caller_roots_noalloc<'a>(gc: &'a Runtime, x: ValueRef, _y: ValueRef) -> Value<'a> {
    CAMLvalues!(gc, x, _y); // But is a macro really necessary?
    x.value(gc).get(0)
}

// A function that takes two arguments and performs allocations
pub fn caller_roots_alloc<'a>(gc: &'a mut Runtime, x: ValueRef, _y: ValueRef) -> Value<'a> {
    // CAMLvalues!(gc, x, _y); // Counterproductive (compilation error)
    caml_alloc_pair(gc);
    x.value(gc).get(0)
}

// Calling an allocating function in the caller-root convention
pub fn call_caller_roots(gc: &mut Runtime) {
    let x = caml_alloc_pair(gc);
    let x_rooted = BoxRoot::create(x);
    caller_roots_alloc(gc, &x_rooted, &x_rooted);
}

// More advanced: a function that does not interact with the runtime
// at all, only accesses values.
pub fn caller_roots_readonly<'a>(x: Value<'a>, y: Value<'a>) -> Value<'a> {
    if x.size() > 0 {
        x.get(0)
    } else {
        y
    }
}

// But can we avoid the Value type entirely?

// One can allocate and return a BoxRoot, which fits the pass-by-root
// approach, but can be more expensive than desired.


/// IV.3. Unrooted values as ValueRef

// ValueCell guarantees that its contents cannot be mutated by someone
// holding a ValueRef, so we can cast a &Value into a ValueRef and
// know that nobody is going to mutate it (which would be UB because
// there can be surviving copies of the &Value).

impl ValueCell {
    pub unsafe fn create_ref(val: *const usize) -> ValueRef<'static> {
        &*(val as *const ValueCell)
    }

    pub unsafe fn create_ref_unrooted(_gc: &Runtime, val: *const usize) -> ValueRef {
        ValueCell::create_ref(val)
    }
}

impl<'a> Value<'a> {
    pub fn as_ref<'b>(&'b self) -> ValueRef<'b> where 'a : 'b {
        let ptr = &self.raw as *const usize;
        unsafe { ValueCell::create_ref(ptr) }
    }

    // In fact, we can point directly into the OCaml heap with the
    // same rules as unrooted ValueRefs obtained via as_ref above.
    // Here, the UnsafeCell expresses that mutation can happen through
    // the OCaml heap.
    //
    // Note that the child below inherits the lifetime of the borrow
    // of the capability, meaning that ValueRef is considered
    // unrooted. Direct pointers into the OCaml heap can become
    // unreachable from the roots by mutation, but in that case they
    // remain valid until the next GC.

    pub fn at(&self, offset: usize) -> ValueRef<'a> {
        assert!(offset < self.size());
        let ptr = self.raw as *const usize;
        unsafe {
            self.assert_runtime_capability();
            let ptr : *const usize = ptr.offset(offset as isize);
            // The cast into an UnsafeCell simply expresses the shared
            // mutability of the OCaml heap, which implies in
            // particular that direct references into it cannot exist
            // as this would be UB, and that we can mutate it
            // following certain rules.
            ValueCell::create_ref(ptr)
        }
    }
}

// In particular, the programmer can read ValueRefs without having to
// manipulate intermediate Values (in exchange of passing the runtime
// capability).

impl ValueCell {
    pub fn get<'a>(&self, gc: &'a Runtime, offset: usize) -> ValueRef<'a> {
        self.value(gc).at(offset)
    }
}

// This also means that we can return unrooted values as ValueRefs. Of
// course, this is limited to ValueCells that survive the function (but
// we have just found a whole lot of them inside the OCaml heap).

pub fn caller_roots_noalloc2<'a>(gc: &'a Runtime, x: ValueRef, _y: ValueRef) -> ValueRef<'a> {
    x.get(gc, 0)
}

// Note that Rust ensures that all arguments to allocating functions
// are rooted, since by exclusivity, none of the argument's lifetime
// can refer to an immutable borrow of the capability.

pub fn caller_roots_alloc2<'a>(gc: &'a mut Runtime, x: ValueRef, _y: ValueRef) -> ValueRef<'a> {
    caml_alloc_pair(gc);
    x.get(gc, 0) // Correct: x cannot refer to an unrooted value
}

// Note that the value has to survive the function. For instance, one
// cannot return an immediate in this way.


/// IV.4. Putting Values back into the ring

// Remember that manipulating Values directly is supposed to have a
// lighter syntactic footprint, since we only need to pass the
// capability once: when they are created. They are also more
// efficient, since ValueRefs cost an indirection (a real one,
// deprived from possible LLVM's noalias optimisations). In particular
// they are the only option to work with immediates as... immediates.
// Moreover, ValueRefs point to shared values that can be mutated by
// someone else.

// If we go the other direction and would like to support both
// passing-by-value and passing-by-reference, we can introduce some
// syntactic sugar to simplify working directly with them in a
// caller-roots context:

impl<'a> Deref for Value<'a> {
    type Target = ValueCell;

    fn deref(&self) -> ValueRef {
        self.as_ref()
    }
}

pub fn test_deref<'a>(gc: &'a mut Runtime) {
    // For instance one can easily pass immediates
    let val = Value::from_int(42);
    caller_roots_alloc(gc, &val, &val); // magic

    // Despite all this automagic, if you forget that one must pass
    // references, you can count on Rust error messages to help you:
    //```
    //745 |     caller_roots_alloc(gc, val, val); // magic
    //    |                            ^^^
    //    |                            |
    //    |                            expected `&ValueCell`, found struct `Value`
    //    |                            help: consider borrowing here: `&val`
    //```


    // One can also easily pass unrooted non-immediates to
    // non-allocating functions.
    let val2 = unsafe { Value::from_ptr(gc, 0) };
    caller_roots_noalloc(gc, &val, &val2);

    // While still allowing the passing by value
    caller_roots_readonly(val, val2);

    // This starts being a bit of a stretch, but like a good theorem
    // obtained after several lemmas, the next result follows from
    // unavoidable logical consequence:
    //caller_roots_alloc(gc, &val, &val2); // Compilation error: good

    // ...and its statement is succinct:
    //```
    //769 |     let val2 = unsafe { Value::from_ptr(gc, 0) };
    //    |                                         -- immutable borrow occurs here
    //...
    //775 |     caller_roots_alloc(gc, &val, &val2); // Compilation error: good
    //    |     ------------------^^^^^^^^^^^^^^^^^
    //    |     |
    //    |     mutable borrow occurs here
    //    |     immutable borrow later used by call
    //```

    // The programmer fixes this by introducing a root
    let val2_root = BoxRoot::create(val2);
    let val3 = caller_roots_alloc2(gc, &val, &val2_root);

    // Note: Problem 5 is still there of course
    //caller_roots_noalloc(gc, &val3, &val2_root); // Compilation error: bad
    let val3 = unsafe {
        let ptr : *const Value<'static> = val3.cell.get();
        ValueCell::create_ref_unrooted(gc,ptr as *const usize)
    };
    caller_roots_noalloc(gc, &val3, &val2_root);

    // val2 is not longer live
    //caller_roots_noalloc(gc, &val, &val2); // Compilation error: good
}

#[allow(unused_macros)]
macro_rules! recover_alloc_cap_ref {
    ($gc:ident, $x:ident) => (
        let $x : Value = {
            let $x : ValueRef = $x;
            unsafe {
                let ptr : *const Value<'static> = $x.cell.get();
                ValueCell::create_ref_unrooted($gc,ptr as *const usize)
            }
        };
    )
}


/// IV.5. Sources of ValueRef

// ValueRef is a versatile type that abstracts a variety of pointers
// to OCaml values:
//
// Immediates
// Unrooted values
// Values inside the OCaml heap
// Boxed Roots
// Local & Global Roots (not shown)
// maybe more?

/// IV.6. L-values

// By the rules of ValueCell, one cannot mutate it by holding a
// ValueRef. Mutations can only happen by modifying the contents of a
// Value.

pub unsafe fn caml_modify(_dst: *mut Value, _src: Value) {}

impl<'a> Value<'a> {
    pub fn set(&self, offset: usize, newval: Value) {
        assert!(offset < self.size());
        let ptr = self.raw as *mut Value;
        unsafe {
            self.assert_runtime_capability();
            caml_modify(ptr.offset(offset as isize), newval);
        }
    }
}

// So that mutating via a ValueRef incurs two indirections and the
// passing of a capability.

impl ValueCell {
    pub fn set(&self, gc: &Runtime, offset:usize, newval: Value) {
        self.value(gc).set(offset, newval);
    }
}

// Instead, we can introduce a new type for OCaml lvalues and lvalue
// references

#[repr(C)]
pub struct ValueCellMut {
    cell: UnsafeCell<Value<'static>>
}

static_assertions::assert_eq_size!(ValueCellMut, usize);

type ValueRefMut<'a> = &'a ValueCellMut;

impl ValueCellMut {
    pub unsafe fn create_ref(val: *mut usize) -> ValueRefMut<'static> {
        &*(val as *mut ValueCellMut)
    }

    pub fn set(&self, _gc: &Runtime, newval: Value) {
        unsafe { caml_modify(self.cell.get(), newval); }
    }
}

impl Deref for ValueCellMut {
    type Target = ValueCell;

    fn deref<'a>(&'a self) -> ValueRef<'a> {
        // In correct order we would define ValueCell and ValueCellMut
        // differently to avoid the transmute.
        unsafe { std::mem::transmute::<&ValueCellMut, &ValueCell>(self) }
    }
}

// So we can get ValueRefMuts as children of Value and ValueCell

impl<'a> Value<'a> {
    // Again, code duplication can be avoided by defining things the
    // other way around.
    pub fn at_mut(&self, offset: usize) -> ValueRefMut<'a> {
        assert!(offset < self.size());
        let ptr = self.raw as *mut usize;
        unsafe {
            self.assert_runtime_capability();
            let ptr : *mut usize = ptr.offset(offset as isize);
            ValueCellMut::create_ref(ptr)
        }
    }
}

impl ValueCell {
    pub fn get_mut<'a>(&self, gc: &'a Runtime, offset: usize) -> ValueRefMut<'a> {
        self.value(gc).at_mut(offset)
    }
}

// Since it uses caml_modify, ValueRefMut can only point into the
// OCaml heap. It would be unsafe to allow have a ValueRefMut come
// from a Value or from a root (the latter of which can require other
// kinds of mutation : plain assignment,
// caml_modify_generational_root...).


fn main() {
    println!("Hello, world!");
}
